# pyenv_plus_pipenv_setup

Scripts for installing and initial setup of pyenv and pipenv


### Prerequisites
* The following packages will be needed on Debian or Ubuntu
```bash
sudo apt install libbz2-dev libffi-dev libreadline-dev libsqlite0-dev libssl-dev zlib1g-dev
```


### Usage
* To install pyenv and pipenv with Python 3.7.3 as the global (default) Python version.
```bash
bash setup_python_environment.sh
```

* To install pyenv and pipenv with Python 3.7.0 as the global (default) Python version.
```bash
PYTHON_VERSION=3.7.0 bash setup_python_environment.sh
```

This script does not update your bashrc.  You will need to append/add the following to your `~/.bashrc` for the installation to be complete.

```bash
PYENV_ROOT="$HOME/.pyenv"
export PYENV_ROOT
export PATH="$PYENV_ROOT/bin:$PATH"

eval "$(pyenv init -)"
```

* To install additional python versions using pyenv.  NOTE: you must have run `setup_python_environment.sh` before this script will work.
```bash
bash install_python_version.sh --version 3.7.1
```


### Uninstall
```
rm -r ~/.pyenv
```

Then remove the pyenv related settings from `~/.bashrc`

```bash
PYENV_ROOT="$HOME/.pyenv"
export PYENV_ROOT
export PATH="$PYENV_ROOT/bin:$PATH"

eval "$(pyenv init -)"
```
