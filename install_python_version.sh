#!/bin/bash

readonly SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# shellcheck source=./lib/common.sh
source "$SCRIPT_DIR/lib/common.sh"
# shellcheck source=./lib/error_handling.sh
source "$SCRIPT_DIR/lib/error_handling.sh"
# shellcheck source=./lib/python/environment.sh
source "$SCRIPT_DIR/lib/python/environment.sh"
# shellcheck source=./lib/python/argument_parser.sh
source "$SCRIPT_DIR/lib/python/argument_parser.sh"
# shellcheck source=./lib/python/pyenv.sh
source "$SCRIPT_DIR/lib/python/pyenv.sh"
# shellcheck source=./lib/python/python.sh
source "$SCRIPT_DIR/lib/python/python.sh"

ARGS=("$@")


main() {
    parse_args "${ARGS[@]}"

    if ! pyenv_is_in_path; then
        except 1 'Unable to find PyENV'
    fi

    initialize_pyenv

    if ! python_version_is_installed "$PYTHON_VERSION"; then
        install_python_version "$PYTHON_VERSION"
    fi
}

main "${ARGS[@]}"
exit 0
