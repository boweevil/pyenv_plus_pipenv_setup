#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

readonly SCRIPT_NAME="$( basename "$0" )"
export SCRIPT_NAME
