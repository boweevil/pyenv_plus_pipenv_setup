#!/bin/bash
trap exit_trap EXIT
trap err_trap ERR
showed_traceback=f


except() {
    local exit_status message
    if is_integer "$1"; then
        exit_status="${1:-1}"
        message="${2:-}"
    else
        exit_status=1
        message="${1:-}"
    fi

    trap - EXIT

    traceback 1

    if [[ -n "${message}" ]]; then
        printf '%s: Exiting with status: %s\n' "${message}" "${exit_status}"
        return "${exit_status}"
    fi

    printf 'Exiting with exit_status: %s\n' "${exit_status}"
    return "${exit_status}"
}

exit_trap() {
    local ec
    ec="$?"
    if [[ $ec != 0 && "${showed_traceback}" != t ]]; then
        traceback 1
    fi
}

err_trap() {
    local ec cmd
    ec="$?"
    cmd="${BASH_COMMAND:-unknown}"
    traceback 1
    showed_traceback=t
    echo "The command ${cmd} exited with exit code ${ec}." 1>&2
}

traceback() {
    # Hide the traceback() call.
    local -i start=$(( ${1:-0} + 1 ))
    local -i end=${#BASH_SOURCE[@]}
    local -i i=0
    local -i j=0

    traceback_output=()
    echo "Traceback:" 1>&2
    for ((i=start; i < end; i++)); do
        j=$(( i - 1 ))
        local function file line
        function="${FUNCNAME[$i]}"
        file="$( realpath "${BASH_SOURCE[$i]}" )"
        line="${BASH_LINENO[$j]}"
        traceback_output+=("${function}() in ${file}:${line}")
    done
    printf '    %s\n' "${traceback_output[@]}" | tac
}


is_integer() {
    if [[ $1 =~ ^[0-9]+$ ]] ; then
       return 0
    fi

    return 1
}
