#!/bin/bash
parse_args() {
    while [[ -n "${1:-}" ]]; do
        case "${1:-}" in
            '-h' | '--help' )
                usage
                exit 0
                ;;
            '-v' | '--version' )
                shift
                PYTHON_VERSION="$1"
                ;;
            * )
                usage
                exit 1
        esac
        shift
    done
    return 0
}


usage() {
    printf '%s\n' "USAGE: $SCRIPT_NAME: [[-h][-v VERSION]]"
    printf '%s\n' "Run with no arguments to install default version: $PYTHON_VERSION"
    printf '\n'
    printf '%s\n' "Example:"
    printf '    %s\n' "$SCRIPT_NAME -v 3.7.3"
}
