#!/bin/bash
pyenv_is_installed() {
    if [ -d "$PYENV_ROOT" ]; then
        return 0
    fi

    return 1
}


install_pyenv() {
    local pyenv_url
    pyenv_url='https://github.com/pyenv/pyenv.git'
    git clone "${pyenv_url}" "$PYENV_ROOT" \
        || except 1 "Failed to download pyenv from ${pyenv_url}."
}


pyenv_is_in_path() {
    if command -v pyenv 1>/dev/null 2>&1; then
        return 0
    fi

    return 1
}


initialize_pyenv() {
    eval "$(pyenv init -)"
}
