#!/bin/bash
readonly PYENV_ROOT="$HOME/.pyenv"
export PYENV_ROOT
export PATH="$PYENV_ROOT/bin:$PATH"
