#!/bin/bash
pipenv_is_installed() {
    if command -v pipenv 1>/dev/null 2>&1; then
        return 0
    fi

    return 1
}


install_pipenv() {
    pip install -U pipenv \
        || except 1 "Failed to install $_"
}
