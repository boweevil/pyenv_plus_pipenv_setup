#!/bin/bash
python_version_is_installed() {
    if pyenv versions | grep "$1" 1>/dev/null 2>&1; then
        return 0
    fi

    return 1
}


install_python_version() {
    CONFIGURE_OPTS='--enable-shared --enable-loadable-sqlite-extensions' \
        pyenv install -v "$1" \
            || except 1 "Failed to install Python $_"
}


set_global_python_version() {
    if [[ ! $(pyenv global) = "$1" ]]; then
        pyenv global "$1" \
            || except 1 "Failed to set global Python $_"
    fi
}


upgrade_pip() {
    pip install --upgrade pip \
        || except 1 "Failed to upgrade pip"
}
