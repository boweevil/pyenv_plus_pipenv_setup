#!/bin/bash

readonly SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# shellcheck source=./lib/common.sh
source "$SCRIPT_DIR/lib/common.sh"
# shellcheck source=./lib/error_handling.sh
source "$SCRIPT_DIR/lib/error_handling.sh"
# shellcheck source=./lib/python/environment.sh
source "$SCRIPT_DIR/lib/python/environment.sh"
# shellcheck source=./lib/python/pyenv.sh
source "$SCRIPT_DIR/lib/python/pyenv.sh"
# shellcheck source=./lib/python/python.sh
source "$SCRIPT_DIR/lib/python/python.sh"
# shellcheck source=./lib/python/pipenv.sh
source "$SCRIPT_DIR/lib/python/pipenv.sh"

if [[ -z "${PYTHON_VERSION:-}" ]]; then
    PYTHON_VERSION=3.7.3
fi


main() {
    if ! pyenv_is_installed; then
        install_pyenv
    fi

    if pyenv_is_in_path; then
        initialize_pyenv
    fi

    if ! python_version_is_installed "${PYTHON_VERSION:-}"; then
        install_python_version "${PYTHON_VERSION:-}"
    fi

    set_global_python_version "${PYTHON_VERSION:-}"

    upgrade_pip

    if ! pipenv_is_installed; then
        install_pipenv
    fi
}

main
exit 0
